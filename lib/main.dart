import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tugas_flutter/translation/locale_keys.g.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
      supportedLocales: [
        Locale('en-UK'),
        Locale('es-SP'),
      ],
      path: "assets/translations",
      startLocale: Locale('en-UK'),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: Mainpage(),
    );
  }
}

class Mainpage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          
          title: Text("easy_localization", ),
          backgroundColor: Colors.blueGrey,
          
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      LocaleKeys.title,
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ).tr(),
                    width: 350,
                    height: 70,
                    color: Colors.red,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      LocaleKeys.app_local_demo,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ).tr(),
                    width: 350,
                    height: 100,
                    color: Colors.yellow,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      LocaleKeys.demo_details,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ).tr(),
                    width: 350,
                    height: 100,
                    color: Colors.green,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      context.setLocale(
                        Locale("en-UK"),
                      );
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
                    
                    child: Row(
                      children: [
                        Image.asset("assets/icons/uk.png",width: 20,height: 20,),
                        SizedBox(width: 5,),
                        Text("English"),
                        
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      context.setLocale(
                        Locale("es-SP"),
                      );
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
                    child: Row(
                      children: [
                        Image.asset("assets/icons/es.png",width: 20,height: 20,),
                        SizedBox(width: 5,),
                        Text("Espanyol"),
                        
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                        onTap: () {
                           print("Youtube");
                        },
                        child: FaIcon(FontAwesomeIcons.youtube, color: Colors.red,size: 50,),
                      ),
                      GestureDetector(
                        onTap: () {
                          print("Facebook");
                        },
                        child: FaIcon(FontAwesomeIcons.facebook,color: Colors.blue, size: 50,),
                      ),
                      GestureDetector(
                        onTap: () {
                           print("Twitter");
                        },
                        child: FaIcon(FontAwesomeIcons.twitter, color: Colors.lightBlue,size: 50,),
                      ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
